from tkinter import *
from tkinter import messagebox
from tkinter import simpledialog
from turtle import bgcolor, color
from PIL import ImageTk, Image


def bloquear():
    for i in range (0,9):
        listaBotones[i].config(state='disable')

def iniciari ():
    for i in range (0,9):
        listaBotones[i].config (state="normal")
        listaBotones[i].config (bg="lightgray")
        listaBotones[i].config (text="")
        tablero[i] = "N"

    global jugador1, jugador2
    jugador1 = simpledialog.askstring("jugador", "Nick del jugador 1: ")
    jugador2 = simpledialog.askstring("jugador", "Nick del jugador 2: ")
    turnojugador.set("Turno de : " + jugador1)

def cambiar (num):
    global turno, jugador1, jugador2
    if tablero[num]== "N" and turno == 0:
        listaBotones[num].config(text="X") 
        listaBotones[num].config(bg="white") 
        tablero[num] = "X"
        turno = 1
        turnojugador.set("Turno de :  " + jugador2)
    elif tablero[num]=="N" and turno == 1:
        listaBotones[num].config(text="O") 
        listaBotones[num].config(bg="lightblue") 
        tablero[num] = "O"
        turno = 0
        turnojugador.set("Turno de :  " + jugador1)
    listaBotones[num].config(state="disable")
    comprobar()

def comprobar():
    if (tablero[0]=="X" and tablero[1]=="X" and tablero[2]=="X") or (tablero[3]=="X" and tablero[4]=="X" and tablero[5]=="X") or (tablero[6]=="X" and tablero[7]=="X" and tablero[8]=="X") :
        bloquear()
        messagebox.showinfo("Ganador","Ha ganado el jugador: " + jugador1)
    elif (tablero[0]=="X" and tablero[3]=="X" and tablero[6]=="X") or (tablero[1]=="X" and tablero[4]=="X" and tablero[7]=="X") or (tablero[2]=="X" and tablero[5]=="X" and tablero[8]=="X") :
        bloquear()
        messagebox.showinfo("Ganador","Ha ganado el jugador: " + jugador1)
    elif (tablero[0]=="X" and tablero[4]=="X" and tablero[8]=="X") or (tablero[2]=="X" and tablero[4]=="X" and tablero[6]=="X"):
        bloquear()
        messagebox.showinfo("Ganador","Ha ganado el jugador: " + jugador1)
    elif (tablero[0]=="O" and tablero[1]=="O" and tablero[2]=="O") or (tablero[3]=="O" and tablero[4]=="O" and tablero[5]=="O") or (tablero[6]=="O" and tablero[7]=="O" and tablero[8]=="O") :
        bloquear()
        messagebox.showinfo("Ganador","Ha ganado el jugador: " + jugador2)
    elif (tablero[0]=="O" and tablero[3]=="O" and tablero[6]=="O") or (tablero[1]=="O" and tablero[4]=="O" and tablero[7]=="O") or (tablero[2]=="O" and tablero[5]=="O" and tablero[8]=="O") :
        bloquear()
        messagebox.showinfo("Ganador","Ha ganado el jugador: " + jugador2)
    elif (tablero[0]=="O" and tablero[4]=="O" and tablero[8]=="O") or (tablero[2]=="O" and tablero[4]=="O" and tablero[6]=="O"):
        bloquear()
        messagebox.showinfo("Ganador","Ha ganado el jugador: " + jugador2)
    elif (tablero[0]=="O" and tablero[3]=="O" and tablero[6]=="O") or (tablero[1]=="O" and tablero[4]=="O" and tablero[7]=="O") or (tablero[2]=="O" and tablero[5]=="O" and tablero[8]=="O") :
        bloquear()
        messagebox.showinfo("Ganador","Ha ganado el jugador: " + jugador2)
        

ventana = Tk()
ventana.config(bg="black")
ventana.geometry("400x500")
ventana.title("JUEGO DE 3 EN RAYA")
turno = 0
jugador1 = ""
jugador2 = ""
listaBotones = [] 
tablero = []
turnojugador = StringVar()
image = Image.open("pete.jpg")
image = image.resize((350, 350), Image.ANTIALIAS)
img = ImageTk.PhotoImage(image)
lbl_img = Label(ventana, image = img)
lbl_img.pack()

for i in range(0,9):
    tablero.append("N") 

boton0 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (0))
listaBotones.append(boton0)
boton0.place(x=50,y=40)
boton1 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (1))
listaBotones.append(boton1)
boton1.place(x=165,y=40)
boton2 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (2))
listaBotones.append(boton2)
boton2.place(x=275,y=40)
boton3 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (3))
listaBotones.append(boton3)
boton3.place(x=50,y=150)
boton4 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (4))
listaBotones.append(boton4)
boton4.place(x=165,y=150)
boton5 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (5))
listaBotones.append(boton5)
boton5.place(x=275,y=150)
boton6 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (6))
listaBotones.append(boton6)
boton6.place(x=50,y=255)
boton7 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (7))
listaBotones.append(boton7)
boton7.place(x=165,y=255)
boton8 = Button (ventana, width = 9, height = 3, command = lambda: cambiar (8))
listaBotones.append(boton8)
boton8.place(x=275,y=255)
turnoJ = Label(ventana, bg="Black",fg="white", font='arial 16 bold', textvariable = turnojugador).place(x = 50, y = 450)
iniciar = Button(ventana, bg= 'red', fg='white', font='Verdana 12 bold', text= 'Iniciar Juego', width=15, height=3, command=iniciari).place(x = 110, y = 360)
bloquear()

ventana.mainloop()



